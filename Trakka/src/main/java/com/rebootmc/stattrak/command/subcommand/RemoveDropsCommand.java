package com.rebootmc.stattrak.command.subcommand;

import com.rebootmc.stattrak.StatTrakPlugin;
import com.rebootmc.stattrak.command.SubCommand;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class RemoveDropsCommand extends SubCommand {

    public RemoveDropsCommand(StatTrakPlugin plugin) {
        super(plugin, "trakka.take.drops", "takedrops");
    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String s, String[] args) {
        if (args.length < 3) {
            sender.sendMessage(plugin.getMessage("messages.usageRemoveDrops"));
        } else {
            Player target = Bukkit.getPlayer(args[1]);

            if (target == null) {
                sender.sendMessage(plugin.getMessage("messages.targetNotFound"));
            } else {
                int drops;

                try {
                    drops = Integer.parseInt(args[2]);
                } catch (NumberFormatException ex) {
                    sender.sendMessage(plugin.getMessage("messages.invalidNumber"));
                    return true;
                }
                
                if (drops < 0) {
                    sender.sendMessage(plugin.getMessage("messages.invalidNumber"));
                } else {
                    int current = plugin.getDropsFile().getDrops(target);
                    plugin.getDropsFile().setDrops(target, Math.max(0, current - drops));
                    plugin.getDropsFile().saveConfiguration();

                    sender.sendMessage(plugin.getMessage("messages.takeFixes")
                            .replace("[amount]", Integer.toString(drops))
                            .replace("[player]", target.getName()));
                    target.sendMessage(plugin.getMessage("messages.lostFixes")
                            .replace("[amount]", Integer.toString(drops))
                            .replace("[player]", sender.getName()));
                }
            }
        }
        return false;
    }
}
