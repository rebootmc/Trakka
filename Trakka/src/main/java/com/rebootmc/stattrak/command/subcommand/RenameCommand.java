package com.rebootmc.stattrak.command.subcommand;

import com.rebootmc.stattrak.StatTrakPlugin;
import com.rebootmc.stattrak.command.SubCommand;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

public class RenameCommand extends SubCommand {

    public static final int MAX_NAME_LENGTH = 13;

    public RenameCommand(StatTrakPlugin plugin) {
        super(plugin, "trakka.rename", "rename", "rn");
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (!(sender instanceof Player)) {
            sender.sendMessage(plugin.getMessage("messages.noConsole"));
        } else if (args.length < 2) {
            sender.sendMessage(plugin.getMessage("messages.usageRename"));
        } else {
            Player player = (Player) sender;
            ItemStack item = player.getItemInHand();
            int coins = plugin.getRenameFile().getRedeems(player);
            String name = implode(1, args);

            if (coins <= 0) {
                sender.sendMessage(plugin.getMessage("messages.notEnoughCoins"));
            } else if (!plugin.isStatTracked(item)) {
                sender.sendMessage(plugin.getMessage("messages.hasNoTrakka"));
            } else if (!name.matches("[a-zA-Z0-9 ]+") || name.length() > MAX_NAME_LENGTH) {
                sender.sendMessage(plugin.getMessage("messages.invalidName"));
            } else {
                plugin.getRenameFile().setRedeems(player, coins - 1);
                plugin.getRenameFile().saveConfiguration();
                player.setItemInHand(plugin.changeName(item, name));
                sender.sendMessage(plugin.getMessage("messages.itemRenamed"));
            }
        }
        return true;
    }

    /**
     * Implode a string array from a starting index and combine into a string separated by a space
     *
     * @param startIndex The start index of the String array to implode
     * @param toImplode  A string array to implode
     * @return An imploded string joined by the spacer
     */
    public static String implode(int startIndex, String[] toImplode) {
        StringBuilder ret = new StringBuilder();

        for (int i = startIndex ; i < toImplode.length ; i++) {
            if (toImplode[i] != null) {
                ret.append(toImplode[i]);
            }
            if (i < toImplode.length - 1) {
                ret.append(" ");
            }
        }

        return ret.toString().trim();
    }
}
