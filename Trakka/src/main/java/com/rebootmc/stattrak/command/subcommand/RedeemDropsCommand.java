package com.rebootmc.stattrak.command.subcommand;

import com.rebootmc.stattrak.StatTrakPlugin;
import com.rebootmc.stattrak.command.SubCommand;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.List;
import java.util.regex.Matcher;

public class RedeemDropsCommand extends SubCommand {
    
    StatTrakPlugin plugin;
    
    public RedeemDropsCommand(StatTrakPlugin plugin) {
        super(plugin, "trakka.redeem.drops", "redeemdrops");
        this.plugin = plugin;
    }
    
    @Override
    public boolean onCommand(CommandSender cs, Command cmd, String label, String[] args) {
        if(cs instanceof Player) {
            Player p =  (Player) cs;
            if(plugin.getDropsFile().getDrops(p)<=0) {
                p.sendMessage(plugin.getMessage("messages.notEnoughCoins"));
                return true;
            }
            ItemStack is = p.getItemInHand();
            if(!plugin.isStatTracked(is)) {
                p.sendMessage(plugin.getMessage("messages.cannotBeTracked"));
                return true;
            }
            List<String> lore = is.getItemMeta().getLore();
            for(int j = 0; j < is.getItemMeta().getLore().size(); j++){
                Matcher match = plugin.DROPS_REGEX.matcher(lore.get(j));
                if(match.matches()) {
                    int i = Integer.parseInt(match.group(1));
                    int x = i + plugin.getConfig().getInt("drops_tokenworth");
                    lore.set(j, lore.get(j).replace(String.valueOf(i), String.valueOf(x)));
                    ItemMeta im = is.getItemMeta();
                    im.setLore(lore);
                    is.setItemMeta(im);
                    plugin.getDropsFile().setDrops(p, plugin.getDropsFile().getDrops(p)-1);
                }
            }
            p.sendMessage(plugin.getMessage("messages.addDrops"));
            return true;
        }
        return false;
    }
}
