package com.rebootmc.stattrak.command.subcommand;

import com.rebootmc.stattrak.StatTrakPlugin;
import com.rebootmc.stattrak.command.SubCommand;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;

public class AddDropsCommand extends SubCommand {

    StatTrakPlugin plugin;

    public AddDropsCommand(StatTrakPlugin plugin) {
        super(plugin, "trakka.give.drops", "givedrops");
        this.plugin = plugin;
    }
    @Override
    public boolean onCommand(CommandSender cs, Command cmd, String label, String[] args) {
        if(args.length < 3) {
            cs.sendMessage(plugin.getMessage("messages.usageAddDrops"));
        }else {
            String player = args[1];
            String amoumnt = args[2];
            int amount;

            try {
                amount = Integer.parseInt(amoumnt);
            }catch(NumberFormatException e) {
                cs.sendMessage(plugin.getMessage("messages.invalidNumber"));
                return true;
            }
            OfflinePlayer target = Bukkit.getOfflinePlayer(player);
            if(target == null || !target.hasPlayedBefore()) {
                cs.sendMessage(plugin.getMessage("messages.targetNotFound"));
                return true;
            }

            if(amount < 0) {
                cs.sendMessage(plugin.getMessage("messages.invalidNumber"));
                return true;
            }

            int current = plugin.getDropsFile().getDrops(target);
            plugin.getDropsFile().setDrops(target, current + amount);

            cs.sendMessage(plugin.getMessage("messages.addDrops")
                    .replace("[amount]", Integer.toString(amount))
                    .replace("[player]", target.getName()));

            if(target.isOnline()) {
                target.getPlayer().sendMessage(plugin.getMessage("messages.receivedDrops")
                        .replace("[amount]", Integer.toString(amount))
                        .replace("[player]", cs.getName()));
            }

        }
        return false;
    }
}
