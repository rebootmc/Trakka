package com.rebootmc.stattrak.command.subcommand;

import com.rebootmc.stattrak.StatTrakPlugin;
import com.rebootmc.stattrak.command.SubCommand;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

public class ClaimCommand extends SubCommand {

    public ClaimCommand(StatTrakPlugin plugin) {
        super(plugin, "trakka.claim", "claim");
    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String s, String[] args) {
        if (!(sender instanceof Player)) {
            sender.sendMessage(plugin.getMessage("messages.noConsole"));
        } else {
            Player player = (Player) sender;
            int claims = plugin.getClaimsFile().getClaims(player);

            if (claims <= 0) {
                sender.sendMessage(plugin.getMessage("messages.notEnoughClaims"));
            } else {
                ItemStack item = player.getItemInHand();

                if (plugin.isStatTracked(item)) {
                    player.setItemInHand(plugin.claimItem(item, player));
                    plugin.getClaimsFile().setClaims(player, claims - 1);
                    plugin.getClaimsFile().saveConfiguration();
                    sender.sendMessage(plugin.getMessage("messages.claimed"));
                } else {
                    player.sendMessage(plugin.getMessage("messages.noItem"));
                }
            }
        }
        return false;
    }
}
