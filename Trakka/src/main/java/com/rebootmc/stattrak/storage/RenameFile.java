package com.rebootmc.stattrak.storage;

import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.UUID;

public class RenameFile extends Configuration {

    /**
     * Creates the token file
     *
     * @param plugin The TokenFile plugin
     * @param fileName The name of the file
     */
    public RenameFile(JavaPlugin plugin, String fileName) {
        super(plugin, fileName);
    }

    /**
     * Gets a players token amount
     *
     * @param player The player
     *
     * @return The amount of redeems the player currently has
     */
    public int getRedeems(Player player) {
        return getRedeems(player.getUniqueId());
    }

    /**
     * Gets a players token amount
     *
     * @param uuid The ID of the player
     *
     * @return The amount of redeems the player currently has
     */
    public int getRedeems(UUID uuid) {
        return getInt(uuid.toString());
    }

    /**
     * Sets the amount of redeems a player has
     *
     * @param player The player
     * @param amount The new amount of redeems the player should have
     *
     * @return The amount of redeems the player currently has
     */
    public int setRedeems(Player player, int amount) {
        return setRedeems(player.getUniqueId(), amount);
    }

    /**
     * Sets the amount of redeems a player has
     *
     * @param uuid The ID of the player
     * @param amount The new amount of redeems the player should have
     *
     * @return The amount of redeems the player currently has
     */
    public int setRedeems(UUID uuid, int amount) {
        set(uuid.toString(), amount);
        return getRedeems(uuid);
    }
}
