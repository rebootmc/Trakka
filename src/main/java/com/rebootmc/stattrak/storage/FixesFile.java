package com.rebootmc.stattrak.storage;

import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.UUID;

public class FixesFile extends Configuration {

    public FixesFile(JavaPlugin plugin, String file) {
        super(plugin, file);
    }

    public int getFixes(Player player) {
        return getFixes(player.getUniqueId());
    }


    public int getFixes(UUID uuid) {
        return getInt(uuid.toString());
    }


    public int setFixes(Player player, int amount) {
        return setFixes(player.getUniqueId(), amount);
    }

    public int setFixes(UUID uuid, int amount) {
        set(uuid.toString(), amount);
        return getFixes(uuid);
    }
}
