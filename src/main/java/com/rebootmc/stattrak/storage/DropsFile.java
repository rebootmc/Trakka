package com.rebootmc.stattrak.storage;

import org.bukkit.OfflinePlayer;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.UUID;

public class DropsFile extends Configuration {

    public DropsFile(JavaPlugin plugin, String file) {
        super(plugin, file);
    }

    private int getDrops(UUID uuid) {
        return getInt(uuid.toString());
    }

    public void setDrops(OfflinePlayer p, int amount) {
        setDrops(p.getUniqueId(), amount);
    }

    public int getDrops(OfflinePlayer op) {
        return getDrops(op.getUniqueId());
    }

    private void setDrops(UUID uuid, int amount) {
        set(uuid.toString(), amount);
    }
}
