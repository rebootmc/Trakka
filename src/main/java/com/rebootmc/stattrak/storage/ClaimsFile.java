package com.rebootmc.stattrak.storage;

import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.UUID;

public class ClaimsFile extends Configuration {

    public ClaimsFile(JavaPlugin plugin, String file) {
        super(plugin, file);
    }

    public int getClaims(Player player) {
        return getClaims(player.getUniqueId());
    }


    public int getClaims(UUID uuid) {
        return getInt(uuid.toString());
    }


    public int setClaims(Player player, int amount) {
        return setClaims(player.getUniqueId(), amount);
    }


    public int setClaims(UUID uuid, int amount) {
        set(uuid.toString(), amount);
        return getClaims(uuid);
    }
}
