package com.rebootmc.stattrak.command.subcommand;

import com.rebootmc.stattrak.StatTrakPlugin;
import com.rebootmc.stattrak.command.SubCommand;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class AddStealsCommand extends SubCommand {

    public AddStealsCommand(StatTrakPlugin plugin) {
        super(plugin, "trakka.give.steals", "givesteals");
    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String s, String[] args) {
        if (args.length < 3) {
            sender.sendMessage(plugin.getMessage("messages.usageAddSteals"));
        } else {
            Player target = Bukkit.getPlayer(args[1]);

            if (target == null) {
                sender.sendMessage(plugin.getMessage("messages.targetNotFound"));
            } else {
                int claims;

                try {
                    claims = Integer.parseInt(args[2]);
                } catch (NumberFormatException ex) {
                    sender.sendMessage(plugin.getMessage("messages.invalidNumber"));
                    return true;
                }

                if (claims < 0) {
                    sender.sendMessage(plugin.getMessage("messages.invalidNumber"));
                } else {
                    int current = plugin.getClaimsFile().getClaims(target);
                    plugin.getClaimsFile().setClaims(target, current + claims);
                    plugin.getClaimsFile().saveConfiguration();
                    sender.sendMessage(plugin.getMessage("messages.addSteals")
                            .replace("[amount]", Integer.toString(claims)).replace("[player]", target.getName()));
                    target.sendMessage(plugin.getMessage("messages.receivedSteals")
                            .replace("[amount]", Integer.toString(claims)).replace("[player]", sender.getName()));
                }
            }
        }
        return false;
    }
}
