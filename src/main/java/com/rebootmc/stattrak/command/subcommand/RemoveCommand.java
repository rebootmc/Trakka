package com.rebootmc.stattrak.command.subcommand;

import com.rebootmc.stattrak.StatTrakPlugin;
import com.rebootmc.stattrak.command.SubCommand;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class RemoveCommand extends SubCommand {

    public RemoveCommand(StatTrakPlugin plugin) {
        super(plugin, "trakka.take", "take");
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (args.length < 3) {
            sender.sendMessage(plugin.getMessage("messages.usageRemove"));
        } else {
            Player target = Bukkit.getPlayer(args[1]);

            if (target == null) {
                sender.sendMessage(plugin.getMessage("messages.targetNotFound"));
            } else {
                int coins;

                try {
                    coins = Integer.parseInt(args[2]);
                } catch (NumberFormatException ex) {
                    sender.sendMessage(plugin.getMessage("messages.invalidNumber"));
                    return true;
                }

                if (coins < 0) {
                    sender.sendMessage(plugin.getMessage("messages.invalidNumber"));
                } else {
                    int current = plugin.getCoinsFile().getCoins(target);
                    plugin.getCoinsFile().setCoins(target, Math.max(0, current - coins));
                    plugin.getCoinsFile().saveConfiguration();
                    sender.sendMessage(plugin.getMessage("messages.takeCoins")
                            .replace("[amount]", Integer.toString(coins)).replace("[player]", target.getName()));
                    target.sendMessage(plugin.getMessage("messages.lostCoins")
                            .replace("[amount]", Integer.toString(coins)).replace("[player]", sender.getName()));
                }
            }
        }
        return true;
    }
}
