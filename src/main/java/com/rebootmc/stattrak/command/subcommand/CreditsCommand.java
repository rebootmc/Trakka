package com.rebootmc.stattrak.command.subcommand;

import com.rebootmc.stattrak.StatTrakPlugin;
import com.rebootmc.stattrak.command.SubCommand;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class CreditsCommand extends SubCommand {

    public CreditsCommand(StatTrakPlugin plugin) {
        super(plugin, "trakka.coins", "coins");
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (args.length < 2 && !(sender instanceof Player)) {
            sender.sendMessage(plugin.getMessage("messages.usageCredits"));
        } else {
            Player target = args.length < 2 ? (Player) sender : Bukkit.getPlayer(args[1]);

            if (target != sender && !sender.hasPermission("trakka.credits.others")) {
                sender.sendMessage(plugin.getMessage("messages.permission"));
            } else if (target == null) {
                sender.sendMessage(plugin.getMessage("messages.targetNotFound"));
            } else {
                int coins = plugin.getCoinsFile().getCoins(target);
                int renames = plugin.getRenameFile().getRedeems(target);
                int drops = plugin.getDropsFile().getDrops(target);
                int fixes = plugin.getFixesFile().getFixes(target);
                int steals = plugin.getClaimsFile().getClaims(target);
                sender.sendMessage(plugin.getMessage("messages.credits")
                        .replace("[player]", target.getName())
                        .replace("[redeems]", Integer.toString(coins))
                        .replace("[renames]", Integer.toString(renames))
                        .replace("[drops]", Integer.toString(drops))
                        .replace("[fixes]", Integer.toString(fixes))
                        .replace("[steals]", Integer.toString(steals)));
            }
        }
        return true;
    }
}
