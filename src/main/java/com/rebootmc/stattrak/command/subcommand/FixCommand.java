package com.rebootmc.stattrak.command.subcommand;

import com.rebootmc.stattrak.StatTrakPlugin;
import com.rebootmc.stattrak.command.SubCommand;
import com.rebootmc.stattrak.storage.FixesFile;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

public class FixCommand extends SubCommand {

    public FixCommand(StatTrakPlugin plugin) {
        super(plugin, "trakka.fix", "fix");
    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String s, String[] args) {
        if (!(sender instanceof Player)) {
            sender.sendMessage(plugin.getMessage("messages.noConsole"));
        } else {
            FixesFile file = plugin.getFixesFile();
            Player player = (Player) sender;
            ItemStack item = player.getItemInHand();

            if (plugin.isStatTracked(item)) {
                if (item.getDurability() != 0) {
                    player.setItemInHand(plugin.fixItem(item));
                    player.sendMessage(plugin.getMessage("messages.fixed"));
                } else {
                    player.sendMessage(plugin.getMessage("messages.noFix"));
                }
            } else {
                player.sendMessage(plugin.getMessage("messages.noItem"));
            }
        }
        return false;
    }
}
