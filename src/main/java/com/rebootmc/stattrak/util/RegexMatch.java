package com.rebootmc.stattrak.util;

public class RegexMatch {
    private String ownerText, killsLoreText, blocksBrokenText, durabilityText, mobKillsText, dropsText;

    public void setBlocksBrokenText(String blocksBrokenText) {
        this.blocksBrokenText = blocksBrokenText;
    }

    public void setDurabilityText(String durabilityText) {
        this.durabilityText = durabilityText;
    }

    public void setKillsLoreText(String killsLoreText) {
        this.killsLoreText = killsLoreText;
    }

    public void setOwnerText(String ownerText) {
        this.ownerText = ownerText;
    }

    public String getBlocksBrokenText() {
        return blocksBrokenText;
    }

    public String getDurabilityText() {
        return durabilityText;
    }

    public String getKillsLoreText() {
        return killsLoreText;
    }

    public String getOwnerText() {
        return ownerText;
    }

    public String getMobKillsText() {
        return mobKillsText;
    }

    public void setMobKillsText(String mobKillsText) {
        this.mobKillsText = mobKillsText;
    }

    public String getDropsText() {
        return dropsText;
    }

    public void setDropsText(String dropsText) {
        this.dropsText = dropsText;
    }
}
